<link rel="stylesheet" href="wp-content/themes/twentyseventeen/mainStyle.css">
<head>
    <title><?php bloginfo( 'name' ); ?></title>
</head>
<?php
$page = $_GET['page'];
$postPerPage = 8;
function url()
{
    return sprintf(
        "%s://%s",
        isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
        $_SERVER['SERVER_NAME']
    );
}

$args = array(
    'post_type' => 'product',
    'posts_per_page' => $postPerPage,
    'offset' => $page
);
$loop = new WP_Query($args);
$baseUrl = url();

function getPaginationLink($baseUrl, $i)
{
    return "<a href=\"" . getPageUrl($baseUrl, $i) . "\">" . $i . "</a>";
}

function getPageUrl($baseUrl, $page)
{
    return $baseUrl . '?page=' . $page;
}

function getPaginationPanel($loop, $page, $baseUrl)
{
    $paginationPanelHtml = '<div class="pagination clearfix">';
    for ($i = 0; $i <= $loop->max_num_pages; $i++) {
        if ($i == $page) {
            $paginationPanelHtml .= '<strong>' . $i . '</strong>';
        } else {
            $paginationPanelHtml .= getPaginationLink($baseUrl, $i);
        }
    }
    return $paginationPanelHtml . '</div>';
}

function getProduct($product)
{
    return "<a class='square' href='" . $product->guid . "'>
            <img src='" . get_the_post_thumbnail_url($product->ID, 'full') . "'/>
            <div class='description'><p>" . $product->post_title . "</p></div>
        </a>";
}


function getRow($row)
{
    $rowHtml = '';
    foreach ($row as $product) {
        $rowHtml .= getProduct($product);
    }
    return '<div class="row">' . $rowHtml . '</div>';
}


function getAllRows($products)
{
    $rows = array_chunk($products, 4);
    $allRowsHtml = '';
    foreach ($rows as $row) {
        $allRowsHtml .= getRow($row);
    }
    return $allRowsHtml;
}

?>
<header>
    <h2><?php bloginfo( 'description' ); ?></h2>
</header>

<?php echo getPaginationPanel($loop, $page, $baseUrl) . '<div class=\'main\'>' . getAllRows($loop->posts) . '</div>' ?>

<?php get_footer(); ?>
<?php show_admin_bar( false ); ?>